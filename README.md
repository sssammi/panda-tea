# Panda Tea

Tired of waiting in long line ups for the new featured drinks? Tired of squinting at the menu boards? Tired of standing behind people who don’t know what they want to order? The Panda Tea web app allows you to browse and order bubble tea from the comfort of your phone or laptop. Drinks ready for pick up right when you arrive! 

## Getting Started

1. Ensure you have the latest version of the code.  
```
	git clone https://sssammi@bitbucket.org/sssammi/panda-tea.git
```
2. In case of errors while running, take a look at the exceptions.  

### Prerequisites

The web application was created in Visual Studio 2019. It utilizes .NET Core 2.2.402 MVC. 

### Installing

1. Download Visual Studio 2019: [Download here](https://visualstudio.microsoft.com/vs/)

## Running the tests

From Visual Studio 2019, follow these steps to run the tests.

1. Open the Test Explorer 
2. Right-click the Tests folder to run all tests

## Deployment

### How to Create the Database 

To have a local database for this project: 

1. Connect to a local server. 
2. In the panda-tea project folder, open createDB_PandaTeaDB.sql in SSMS.  
3. Execute the CREATE DATABASE line. 
4. Execute the rest of the SQL file. 

### How to Install and Configure the Web Site 

To connect to the Azure-hosted database:

Once ready to push to production: 
1. Change Environment Variable “ASPNETCORE_ENVIRONMENT” from Development to Production. 
2. Ensure Azure SQL Database (pandateadb) is added as a Dependency 
3. On the menu go to Build > Publish PandaTea
4. Click Publish. 

Access the website via web browser: 
1. Open preferred web browser. 
2. Direct to URL: https://pandatea.azurewebsites.net/ 

## Versioning

We use [GitHub](https://github.com/) for versioning. 

## Authors

* **Irene Kwon, Dennis Nay, Jaden Ahn, Sammi Dang** - [Panda Tea](https://github.com/irnkwon/panda-tea)

See also the list of [contributors](https://github.com/irnkwon/panda-tea/graphs/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details. We chose to use an MIT License because it allows us to share the code under a copyleft license without forcing others to expose their proprietary code, it's business friendly and open-source friendly while still allowing for monetization.

## Acknowledgments

Thank you to our professor Yash Shah for continued support with this project. 